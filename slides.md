% Mon wiki va mal :broken_heart:
% Quelles alternatives pour une base de connaissance ?

# :books: Une base de quoi ?

## De l'utile !

- :notebook: Procédures
- :toolbox: Outils utilisés
- :gift: Description du produit
- :rocket: Description de nos objectifs

## Des garanties !

- :handshake: Fiable
- :ring: Unique

## De l'utilisable !

- :door: Accéder
- :detective: Explorer
- :mag: Rechercher
- :pencil2: Editer
- :factory: Organiser
- :white_check_mark: Valider

# :t-rex: REX Wiki Circle-CAD

## Citation d'un illustre anonyme

> Va voir le canal du pays des bains chauds, y'a le lien dans l'en-tête

## Citation d'un anonyme

> Ah zut, le lien est plus valide, bon je vais demander à xxxx

## Citation

> Atta, cette page là je la rajoute tout de suite à mes bookmarks

## :scream: S'informer nuit gravement à la santé

- [L'outil de recherche est touffu](https://gitlab.com/search?search=pipeline&nav_source=navbar&project_id=27139494&group_id=12356876&scope=wiki_blobs)
- [La table des matières est inutilisable](https://gitlab.com/anatoscope/circle/organisation/wiki/-/wikis/home)

## :arrows_counterclockwise: Les itérations sont fastidieuses

- Les ajouts sont faciles **MAIS**
- La *relecture* par un pair est compliquée

## :fire: Pour résumer ce qui est *délicat*

- L'absence de processus de relecture par un pair
- Les fonctionnalités de recherches dysfonctionnelles
- La mise en page écrasante

# :bulb: Une alternative ?

:mage: Et un site statique, ça ferait pas l'affaire ?

:exploding_head: Ma voui kcé une bonne idée ça

## :rocket: On test ?

[Demo](https://manteapi.gitlab.io/handbook-sandbox/)

## :thinking: Résumé

- :book: [Template de handbook](https://gitlab.com/brownfield-dev/remote/handbook) basé sur le framework [Hugo](https://gohugo.io/)
- :rocket: Intégration dans l'outil CI/CD de Gitlab pour la publication
- :eyeglasses: Intégration dans l'outil Gitlab pour le cycle de relecture
- :zap: Recherche incrémentale && fonctionnelle.

## :fox_face: Fun fact: Recommandé par *Gitlab*

TL; DR: Ergonomie, cycle de relecture, passage à l'échelle

- [Why wiki handbooks don't scale ?](https://about.gitlab.com/handbook/handbook-usage/#wiki-handbooks-dont-scale/)
- [Handbook first documentation](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/)

*disclaimer: C'est pas parce que les autres le font que ...*

## :tooth: Func fact: Un concurrent

[3DME](https://imagoworks.gitbook.io/3dme-crown/welcome/introduction)

*disclaimer: C'est pas parce que les autres le font que ...*

# :pray: Conclusion

- ~~:smiling_imp: Si on brûlait notre wiki ?~~
- :heart: Si on passait à un site statique ?
- :dollar: Faible coût pour transférer notre wiki
- :rocket: Applicable à d'autres projets
  - un manuel utilisateur avec de la génération automatique de contenu ?
